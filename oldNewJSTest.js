import { test, expect } from '@playwright/test';

test('has title', async ({ page }) => {
  await page.goto('https://playwright.dev');
  const title = page.locator('title');
  await expect(title).toHaveText('Playwright');
});
